const socket = io()

socket.on("connect", () => {
  console.log("Conectado al servidor")
})

socket.on("disconnect", () => {
  console.log("Perdimos conexion con el servidor")
})

/* Enviar informacion */
socket.emit(
  "enviarMensaje", 
  {
    user: "Jean Marco",
    mensaje: "Este es mi mensaje"
  }
)

/* Escuchar informacion */
socket.on("enviarMensaje", mensaje => {
  console.log("Servidor mensaje: ", mensaje)
})