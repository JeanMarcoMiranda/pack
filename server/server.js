//Librerias
const express = require('express');
const path = require('path');
const app = express();
const socketIO = require('socket.io')
const http = require('http')
const morgan = require('morgan');
const { Usuarios } = require('./classes/usuarios');
const { crearMensaje } = require('./utils/utilities')


const server = http.createServer(app)

const publicPath = path.resolve(__dirname, '../public');
const port = process.env.PORT || 3000;

app.use(express.static(publicPath));
app.use(morgan('dev'))


/* Comunicacion entre Backet y Socket.io */
const io = socketIO(server)

const usuarios = new Usuarios()

io.on("connection", client => {

    client.on("entrarChat", (data, cb) => {

        if(!data.nombre) {
            return cb({
                error: true,
                mensaje: "El nombre es necesario"
            })
        }

        let personas = usuarios.agregarPersona(client.id, data.nombre)

        client.broadcast.emit('listaPersonas', usuarios.getPersonas())

        cb(personas)

    })

    client.on('crearMensaje', (data) => {

        console.log("Este es el id del usuario actual: ", client.id)
        console.log("A VER SI ENCUENTRA ", usuarios.getPersona(client.id))
        let persona = usuarios.getPersona(client.id)

        let mensaje = crearMensaje(persona.nombre, data.mensaje)

        client.broadcast.emit('crearMensaje', mensaje)

    })

    client.on("disconnect", () => {

        let personaBorrada = usuarios.borrarPersona(client.id)
        
        client.broadcast.emit('crearMensaje', {
            usuario: 'Administrador',
            mensaje: crearMensaje('Admin', `${personaBorrada.nombre} salio`)
        })

        client.broadcast.emit('listaPersonas', usuarios.getPersonas())
    
    })

    /* client.emit("enviarMensaje",
        {
            user: "Admin",
            mensaje: "Bienvenido a esta aplicacion"
        }
    )

    client.on("disconnect", () => {
        console.log("Usuario desconectado.")
    })

    client.on("enviarMensaje", mensaje => {
        console.log(mensaje)
    })

    client.on("enviarMensaje", (data, callback) => {
        console.log(data)

        client.broadcast.emit("enviarMensaje", data)
    }) */
})

server.listen(port, (err) => {

    if (err) throw new Error(err);

    console.log(`Servidor corriendo en puerto ${port}`);

});